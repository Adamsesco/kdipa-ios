//
//  RequestsSideMenu.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/16/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class RequestsSideMenu: UIView {
    
    // MARK: - Outlets
    
    @IBOutlet weak var itemsStack: UIStackView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerItem: SideMenuItem!
    @IBOutlet weak var vacationsItem: SideMenuItem!
    
    @IBOutlet weak var absenceItem: SideMenuItem!
    @IBOutlet weak var permissionsItem: SideMenuItem!
    @IBOutlet weak var vacationAmedmentItem: SideMenuItem!
    @IBOutlet weak var vacationExtentionItem: SideMenuItem!
    @IBOutlet weak var vacationReturnItem: SideMenuItem!
    @IBOutlet weak var emergencyLeaveItem: SideMenuItem!
    @IBOutlet weak var sickLeaveItem: SideMenuItem!
    @IBOutlet weak var officialDutyItem: SideMenuItem!
    @IBOutlet weak var requestStatusItem: SideMenuItem!
    @IBOutlet weak var btnHideRequests: UIButton!
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "RequestsSideMenu", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
        configureItems()
        localize()
    }
    
    func configureItems() {
        self.headerItem.type = .requests
        self.headerItem.isSelected = true
        for view in self.itemsStack.subviews[1...] {
            (view as? SideMenuItem)?.imgIcon.isHidden = true
            (view as? SideMenuItem)?.type = .sample
        }
    }
    
    func localize() {
        self.vacationsItem.configure(Localizable.RequestSideMenuTitles.vacation)
        self.vacationReturnItem.configure(Localizable.RequestSideMenuTitles.vacationReturn)
        self.vacationAmedmentItem.configure(Localizable.RequestSideMenuTitles.vacationAmendment)
        self.emergencyLeaveItem.configure(Localizable.RequestSideMenuTitles.emergencyLeave)
        self.permissionsItem.configure(Localizable.RequestSideMenuTitles.permission)
        self.vacationExtentionItem.configure(Localizable.RequestSideMenuTitles.vacationExtension)
        self.sickLeaveItem.configure(Localizable.RequestSideMenuTitles.sickLeave)
        self.officialDutyItem.configure(Localizable.RequestSideMenuTitles.officialDuty)
        self.requestStatusItem.configure(Localizable.RequestSideMenuTitles.requestStatus)
        self.absenceItem.configure(Localizable.RequestSideMenuTitles.absence)
    }
}
