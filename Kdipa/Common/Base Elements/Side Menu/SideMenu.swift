//
//  SideMenu.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/15/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class SideMenu: UIView {
    
    // MARK: - Outlets 
    
    @IBOutlet weak var constraintRequestsTrailing: NSLayoutConstraint!
    @IBOutlet weak var vwBlurrBackGround: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var requestsSideMenu: RequestsSideMenu!
    @IBOutlet weak var homeItem: SideMenuItem!
    @IBOutlet weak var transActionItem: SideMenuItem!
    @IBOutlet weak var geoLocationItem: SideMenuItem!
    @IBOutlet weak var requestsItem: SideMenuItem!
    @IBOutlet weak var notificationsItem: SideMenuItem!
    @IBOutlet weak var newsItem: SideMenuItem!
    @IBOutlet weak var settingItem: SideMenuItem!
    @IBOutlet weak var logoutItem: SideMenuItem!
    @IBOutlet weak var itemsStack: UIStackView!
    
    // MARK: - Properties
    
    var coordinator: MenuCoordinator?
    static let shared: SideMenu = SideMenu()
    
    // MARK: - inits
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func sharedInit() {
        let nib = UINib(nibName: "SideMenu", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
        configureSideItems()
        configureItemsDelegate()
        self.requestsSideMenu.btnHideRequests.addTarget(self, action: #selector(self.hideRequestsMenu), for: .touchUpInside)
    }
    
    // MARK: Configure
    
    private func configureSideItems() {
        self.homeItem.type = .home
        self.transActionItem.type = .transaction
        self.geoLocationItem.type = .geolocation
        self.requestsItem.type = .requests
        self.notificationsItem.type = .notification
        self.newsItem.type = .news
        self.settingItem.type = .setting
        self.logoutItem.type = .logout
    }
    
    private func configureItemsDelegate() {
        for view in self.itemsStack.subviews {
            (view as? SideMenuItem)?.delegate = self
        }
        for view in self.requestsSideMenu.itemsStack.subviews {
            (view as? SideMenuItem)?.delegate = self
        }
    }
    
    private func showRequestsMenu() {
        UIView.animate(withDuration: 0.3) {
            self.constraintRequestsTrailing.constant = self.requestsSideMenu.frame.width
            self.layoutIfNeeded()
        }
    }
    
    @objc private func hideRequestsMenu() {
        UIView.animate(withDuration: 0.3) {
            self.constraintRequestsTrailing.constant = 0
            self.layoutIfNeeded()
        }
    }
    
    func toggleRequestMenu() {
        if self.constraintRequestsTrailing.constant == 0 {
            self.showRequestsMenu()
        } else {
            self.hideRequestsMenu()
        }
    }
    func setasSelectedItem(item: SideMenuItem) {
        for view in self.itemsStack.subviews {
            (view as? SideMenuItem)?.isSelected = false
        }
        for view in self.requestsSideMenu.itemsStack.subviews {
            (view as? SideMenuItem)?.isSelected = false
        }
        item.isSelected = true
    }
}

extension SideMenu: SideMenuDelegate {
    func didSelectItem(sender: SideMenuItem) {
        self.setasSelectedItem(item: sender)
        switch sender.type {
        case .requests:
            self.toggleRequestMenu()
        case .home:
            self.coordinator?.dashboard()
        case .geolocation:
            self.coordinator?.geolocation()
        case .sample:
            self.handleRequestItemClick(requestItem: sender)
        default:
            break
        }
    }
    
    func handleRequestItemClick(requestItem: SideMenuItem) {
        print(requestItem.tag)
        switch requestItem.tag {
        case RequestsItemType.emergencyLeave.rawValue:
            self.coordinator?.openEmergencyLeave()
        case RequestsItemType.vacations.rawValue:
            self.coordinator?.openVactions()
        case RequestsItemType.absences.rawValue:
            self.coordinator?.openAbsence()
        case RequestsItemType.permissions.rawValue:
            self.coordinator?.openPermission()
        default:
            break
        }
    }
}
