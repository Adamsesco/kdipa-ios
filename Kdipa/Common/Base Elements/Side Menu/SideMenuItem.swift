//
//  SideMenuItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/15/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol SideMenuDelegate: class {
    func didSelectItem(sender: SideMenuItem)
}

enum RequestsItemType: Int {
    case header = 100
    case permissions = 101
    case vacations = 102
    case vacationAmendment = 103
    case vacationExtention = 104
    case vacationReturn = 105
    case emergencyLeave = 106
    case sickLeave = 107
    case officialDuty = 108
    case requestStatus = 109
    case absences = 110
}

enum SideMenuItemType {
    case sample
    case home
    case transaction
    case geolocation
    case requests
    case notification
    case news
    case setting
    case logout
}

class SideMenuItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgShowMoreIcon: UIImageView!
    
    var isSelected: Bool = false {
        didSet {
            if self.isSelected {
                self.imgIcon.tintColor = UIColor(red: 0/255, green: 83/255, blue: 132/255, alpha: 1)
                self.contentView.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
                self.lblTitle.font = UIFont.frutigerBold(size: 17)
            } else {
                self.imgIcon.tintColor = UIColor.kdipaGray
                self.contentView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
                self.lblTitle.font = UIFont.frutigerRegular(size: 17)
            }
        }
    }
    
    var type: SideMenuItemType = .home {
        didSet {
            configure()
        }
    }
    
    weak var delegate: SideMenuDelegate?
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "SideMenuItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: Configure
    
    func configure(_ title: String = "") {
        self.isSelected = false
        switch type {
        case .home:
            self.imgIcon.image = UIImage(named: IconName.home.rawValue)
            self.lblTitle.text = Localizable.SideMenu.homeTitle.uppercased()
            self.isSelected = true
        case .transaction:
            self.imgIcon.image = UIImage(named: IconName.transaction.rawValue)
            self.lblTitle.text = Localizable.SideMenu.transactionTitle.uppercased()
        case .geolocation:
            self.imgIcon.image = UIImage(named: IconName.fingerprints.rawValue)
            self.lblTitle.text = Localizable.SideMenu.geolocationTitle.uppercased()
        case .requests:
            self.imgIcon.image = UIImage(named: IconName.newrequest.rawValue)
            self.lblTitle.text = Localizable.SideMenu.requestsTitle.uppercased()
            self.imgShowMoreIcon.isHidden = false
        case .notification:
            self.imgIcon.image = UIImage(named: IconName.notifications.rawValue)
            self.lblTitle.text = Localizable.SideMenu.notificationsTitle.uppercased()
        case .news:
            self.imgIcon.image = UIImage(named: IconName.news.rawValue)
            self.lblTitle.text = Localizable.SideMenu.newsTitle.uppercased()
        case .setting:
            self.imgIcon.image = UIImage(named: IconName.setting.rawValue)
            self.lblTitle.text = Localizable.SideMenu.settingTitle.uppercased()
        case .logout:
            self.imgIcon.image = UIImage(named: IconName.logout.rawValue)
            self.lblTitle.text = Localizable.SideMenu.logoutTitle.uppercased()
        case .sample:
            self.lblTitle.text = title
        }
        let itemClickedGesture = UITapGestureRecognizer(target: self, action: #selector(self.didSelectItem))
        self.contentView.addGestureRecognizer(itemClickedGesture)
    }
    
    @objc func didSelectItem() {
        self.delegate?.didSelectItem(sender: self)
    }
}
