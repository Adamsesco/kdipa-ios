//
//  KdipaCheckBox.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol CheckBoxDelegate: class {
    func toggle()
}
@IBDesignable class KdipaCheckBox: UIButton {
    
    @IBInspectable var checkedImage: UIImage? = UIImage(named: "checked_box")
    @IBInspectable var uncheckedImage: UIImage? = UIImage(named: "unchecked_box")
    
    weak var delegate: CheckBoxDelegate?
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.setImage(UIImage(named: "unchecked_box"), for: .disabled)
        self.tintColor = UIColor.kdipaPurple
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
            delegate?.toggle()
        }
    }
}
