//
//  File.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum KdipaLabelSize: Int {
    case extraLarge = 3
    case large = 2
    case medium = 1
    case small = 0
}

class KdipaLabel: UILabel {
    
    // MARK: - Constant
    
    struct Constant {
        static let fontName = ""
    }
    
    // MARK: - Properties
    
    @IBInspectable
    var size: Int = 0 {
        didSet {
            self.type = KdipaLabelSize(rawValue: size) ?? .small
        }
    }
    
    var type: KdipaLabelSize = .small {
        didSet {
            switch type {
            case .extraLarge:
                self.font = UIFont.frutigerBold(size: 28)
            case .large:
                self.font = UIFont.frutigerRegular(size: 18)
            case .medium:
                self.font = UIFont.frutigerRegular(size: 15)
            case .small:
                self.font = UIFont.frutigerRegular(size: 13)
            
            }
        }
    }
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        self.font = UIFont(name: "Frutiger", size: 13)
    }
}
