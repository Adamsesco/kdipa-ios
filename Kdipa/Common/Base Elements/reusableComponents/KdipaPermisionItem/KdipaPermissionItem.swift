//
//  KdipaPermissionItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/28/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class KdipaPermissionItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblRow: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaPermissionItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - Configure
    
    func configure(row: Int, title: String, date: String) {
        self.lblRow.text = "\(row)"
        self.lblTitle.text = title
        self.lblDate.text = date
    }
}
