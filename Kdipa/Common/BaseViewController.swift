//
//  UIViewController + extension.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, Storyboarded {
    
    let container: UIView = UIView()
    let loadingView: UIView = UIView()
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    static let sideMenu = SideMenu.shared
    let sideRequestMenu = RequestsSideMenu()
    
    let confirmationView: RoundedView = RoundedView()
    
    private var isSideMenuHidden = true
    
    private var viewFrame: CGRect {
        return self.navigationController?.view.frame ?? self.view.frame
    }
    
    func setupNavigationBarItems() {
        setupLeftNavItem()
        setupRightNavItems()
        setupRemainingNavItems()
        setupSideBarMenu()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
// MARK: - Side Bar Setup

extension BaseViewController {
    
    private func setupSideBarMenu() {
        let sideMenu = BaseViewController.sideMenu
        sideMenu.frame = CGRect(x: -viewFrame.width, y: 0, width: viewFrame.width, height: viewFrame.height)
        sideRequestMenu.frame = sideMenu.frame
        sideMenu.addSubview(sideRequestMenu)
        self.navigationController?.view.addSubview(sideMenu)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.toggleShowMenu))
        sideMenu.vwBlurrBackGround.addGestureRecognizer(tapGesture)
    }
    
    private func setupRemainingNavItems() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        titleImageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = titleImageView
        
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupLeftNavItem() {
        let menuButton = UIButton(type: .system)
        menuButton.setImage(UIImage(named: "side menu")?.withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(self.toggleShowMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
    }
    
    private func setupRightNavItems() {
        let notificationButton = UIButton(type: .system)
        notificationButton.setImage(#imageLiteral(resourceName: "notifications").withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = UIColor.kdipaDarkBlue
        notificationButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: notificationButton)]
    }
    
    @objc func toggleShowMenu() {
        let sideMenu = BaseViewController.sideMenu
        if isSideMenuHidden {
            UIView.animate(withDuration: 0.3, animations: {
                sideMenu.frame = self.viewFrame
            }, completion: { _ in
                sideMenu.vwBlurrBackGround.alpha = 0.2
            })
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        } else {
            sideMenu.vwBlurrBackGround.alpha = 0
            UIView.animate(withDuration: 0.3) {
                sideMenu.frame = CGRect(x: -self.viewFrame.width, y: 0, width: self.viewFrame.width, height: self.viewFrame.height)
            }
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        self.isSideMenuHidden.toggle()
    }
}
// MARK: - Spinner handlers

extension BaseViewController {
    func showSpinner(uiView: UIView) {
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.black
        container.alpha = 0.3
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.gray
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 50.0, height: 50.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.color = UIColor.kdipaDarkBlue
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2,
                                y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }
    
    func hideSpinner(uiView: UIView) {
        DispatchQueue.main.async {
            self.container.removeFromSuperview()
        }
    }
    
    func showConfirmationMessage(view: UIView, message: String) {
        confirmationView.frame = CGRect(x: 50, y: 25, width: view.frame.width - 100, height: 30)
        confirmationView.backgroundColor = UIColor.gray
        confirmationView.cornerRadius = 15
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: confirmationView.frame.width, height: confirmationView.frame.height))
        label.font = UIFont.frutigerRegular(size: 16)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = message
        confirmationView.addSubview(label)
        confirmationView.alpha = 0
        view.addSubview(confirmationView)
        UIView.animate(withDuration: 1, animations: {
            self.confirmationView.alpha = 0.8
        }, completion: {  _ in
            UIView.animate(withDuration: 0.5, delay: 0.3, animations: {
                self.confirmationView.alpha = 0
            }, completion: { _ in
                self.confirmationView.removeFromSuperview()
            })
        })
        
    }
}
