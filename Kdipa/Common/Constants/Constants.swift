//
//  Constant.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Constants {
    
    static let kdipaEmployeeBaseURL = "http://216.250.119.123:3001"
    static let kdipaInvestorBaseURL = "http://62.150.248.70/kdipaportal"
    static let timeoutSeconds = 30.0
    
    struct UserDefaultsKeys {
        static var email: String = "emailKey"
        static var password: String = "passwordKey"
        static var dashboard: String = "SavedDashboard"
    }
}
