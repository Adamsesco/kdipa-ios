//
//  File.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

enum ApiCallState {
    case inactive
    case running
    case failed
    case success
}
