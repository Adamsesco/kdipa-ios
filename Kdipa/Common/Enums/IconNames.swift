//
//  AssetIconNames.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/16/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

enum IconName: String {
    case home
    case transaction
    case fingerprints
    case newrequest
    case notifications
    case news
    case setting
    case logout
//    case =
}
