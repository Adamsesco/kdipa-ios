//
//  StoryboardsKeys.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

enum StoryboardName: String {
    case main = "Main"
    case menu = "Menu"
}
