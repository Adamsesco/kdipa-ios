//
//  UIColor + extension.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

extension UIColor {
    static var kdipaGray: UIColor {
        return UIColor(named: "kdipaGray")!
    }
    static var kdipaGrayBorder: UIColor {
        return UIColor(named: "kdipaGrayBorder")!
    }
    static var kdipaLightGray: UIColor {
        return UIColor(named: "kdipaLightGray")!
    }
    static var kdipaLightBlue: UIColor {
        return UIColor(named: "kdipaLightBlue")!
    }
    static var kdipaDarkBlue: UIColor {
        return UIColor(named: "kdipaDarkBlue")!
    }
    static var kdipaOrange: UIColor {
        return UIColor(named: "kdipaOrange")!
    }
    static var kdipaPurple: UIColor {
        return UIColor(named: "kdipaPurple")!
    }
}
