//
//  UIFont + Extension.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func frutigerRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Frutiger", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func frutigerBold(size: CGFloat) -> UIFont {
        return UIFont(name: "FrutigerBold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
