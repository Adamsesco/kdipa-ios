//
//  Localizable.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Localizable {
    private init() {
        
    }
}

extension Localizable {
    
    // MARK: - Authentication
    
    struct LoadingView {
        
        // MARK: - Private Init
        
        private init() {}
        
        // MARK: Common
        
        static var loadingTitle: String { return "loader.loadingTitle".localized }
        static var successTitle: String { return "loader.successTitle".localized }
        static var loginTitle: String { return "loader.loginTitle".localized }
    }
    
    struct SideMenu {
        static var homeTitle: String { return "SideMenu/home".localized }
        static var transactionTitle: String { return "SideMenu/transaction".localized }
        static var geolocationTitle: String { return "SideMenu/geolocation".localized }
        static var newsTitle: String { return "SideMenu/news".localized }
        static var requestsTitle: String { return "SideMenu/newrequest".localized }
        static var notificationsTitle: String { return "SideMenu/notifications".localized }
        static var settingTitle: String { return "SideMenu/setting".localized }
        static var logoutTitle: String { return "SideMenu/logout".localized }
    }
    
    struct RequestSideMenuTitles {
        
        static var permission: String { return "RequestsSideMenu/permission".localized }
        static var vacation: String { return "RequestsSideMenu/vacation".localized }
        static var vacationAmendment: String { return "RequestsSideMenu/vacationAmendment".localized }
        static var vacationExtension: String { return "RequestsSideMenu/vacationExtension".localized }
        static var vacationReturn: String { return "RequestsSideMenu/vacationReturn".localized }
        static var emergencyLeave: String { return "RequestsSideMenu/emergencyLeave".localized }
        static var sickLeave: String { return "RequestsSideMenu/sickLeave".localized }
        static var officialDuty: String { return "RequestsSideMenu/officialDuty".localized }
        static var requestStatus: String { return "RequestsSideMenu/requestStatus".localized }
        static var absence: String {
            return "RequestsSideMenu/absence".localized
        }
        
    }
    
}
