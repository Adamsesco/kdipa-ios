//
//  Coordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
