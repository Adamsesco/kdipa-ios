//
//  BiometricManager.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-28.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

enum LocationStatus {
    case authorizedAlways
    case notDetermined
    case restricted
    case authorizedWhenInUse
    case denied
}
import CoreLocation

class UserLocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = UserLocationManager()

    // Set the manager object
    let locationManager: CLLocationManager = {
        $0.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        $0.distanceFilter = 1
        $0.requestWhenInUseAuthorization()

        return $0
    }(CLLocationManager())

    let locationStatus: Box<LocationStatus> = Box(.notDetermined)

    private(set) var currentLocation: CLLocationCoordinate2D!
    private(set) var currentHeading: CLHeading!

    private override init() {
        super.init()
        locationManager.delegate = self
    }

    // MARK: - Control Mechanisms

    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }

    func startUpdatingHeading() {
        locationManager.startUpdatingHeading()
    }

    func stopUpdatingHeading() {
        locationManager.stopUpdatingHeading()
    }

    // MARK: Location Updates
    func locationManager(_ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation])
    {
        // If location data can be determined
        if let location = locations.last {
            currentLocation = location.coordinate
        }
        let locationStatus = CLLocationManager.authorizationStatus()
        switch locationStatus {
        case .authorizedAlways:
            self.locationStatus.value = .authorizedAlways
        case .authorizedWhenInUse:
            self.locationStatus.value = .authorizedWhenInUse
        case .notDetermined:
            self.locationStatus.value = .notDetermined
            locationManager.requestAlwaysAuthorization()
        case .restricted:
            self.locationStatus.value = .restricted
            locationManager.requestAlwaysAuthorization()
        case .denied:
            self.locationStatus.value = .denied
            locationManager.requestAlwaysAuthorization()
        @unknown default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager,
        didFailWithError error: Error)
    {
        print("Location Manager Error: \(error)")
    }

    // MARK: Heading Updates
    func locationManager(_ manager: CLLocationManager,
        didUpdateHeading newHeading: CLHeading)
    {
        currentHeading = newHeading
    }

    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return true
    }
}
