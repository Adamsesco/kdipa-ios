//
//  ApplyForUserViewmodel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/11/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class ApplyForUserViewModel {
    var userService: UserService = UserService()
    func createUser(user: User, completion: @escaping ([String: Any]?, Error?) -> Void) {
        userService.createUser(user: user, completion: completion)
    }
}
