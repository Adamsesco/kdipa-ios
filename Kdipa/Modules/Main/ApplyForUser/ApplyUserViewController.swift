//
//  ApplyUserViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class ApplyUserViewController: BaseViewController {
    
    @IBOutlet weak var txtFirstName: KdipaTextField!
    @IBOutlet weak var txtLastName: KdipaTextField!
    @IBOutlet weak var txtEmail: KdipaTextField!
    @IBOutlet weak var txtCompany: KdipaTextField!
    @IBOutlet weak var txtPhoneNumber: KdipaTextField!
    @IBOutlet weak var btnCreateAccount: KdipaButton!
    @IBOutlet weak var constraintBodyBottom: NSLayoutConstraint!
    
    var viewModel: ApplyForUserViewModel =  ApplyForUserViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtFirstName.type = .standard
        self.txtLastName.type = .standard
        self.txtEmail.type = .standard
        self.txtCompany.type = .standard
        self.txtPhoneNumber.type = .standard
        self.btnCreateAccount.type = .filled
        self.txtPhoneNumber.textfield.keyboardType = .numberPad
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let applyTapGuesture = UITapGestureRecognizer(target: self, action: #selector(self.applyForUser))
        self.btnCreateAccount.button.addGestureRecognizer(applyTapGuesture)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func applyForUser() {
//        if !allFieldValidated {
//            return
//        }
        let newUser = User(firstName: self.txtFirstName.text ?? "",
                           lastName: self.txtLastName.text ?? "",
                           account: self.txtEmail.text ?? "",
                           companyName: self.txtCompany.text ?? "",
                           phone: self.txtPhoneNumber.text ?? "")
        self.showSpinner(uiView: self.view)
        self.viewModel.createUser(user: newUser) {_, error in
            DispatchQueue.main.async {
                self.hideSpinner(uiView: self.view)
                if error == nil {
                    let alert = UIAlertController(title: "your account is successfully created", message: "we will active your account in next 24 hours", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true)
                }
            }
        }
        
    }
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.constraintBodyBottom.constant = keyboardHeight + 20
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.constraintBodyBottom.constant = 40
    }
    
}
