//
//  MainCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let signInVc = SignInViewController.instantiate(from: .main)
        signInVc.coordinator = self
        navigationController.pushViewController(signInVc, animated: false)
        signInVc.navigationController?.navigationBar.isHidden = true
    }
    
    func applyForUser() {
        let applyForUserVC = ApplyUserViewController.instantiate(from: .main)
        navigationController.pushViewController(applyForUserVC, animated: true)
        applyForUserVC.navigationController?.navigationBar.isHidden = false
    }
    
    func forgetPassword() {
        
    }
    
    func dashboard() {
        let menuCoordinator = MenuCoordinator(navigationController: self.navigationController)
        menuCoordinator.start()
    }
}
