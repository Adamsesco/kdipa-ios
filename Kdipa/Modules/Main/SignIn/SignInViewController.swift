//
//  SignInViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
        
    @IBOutlet weak var txtFieldUsername: KdipaTextField!
    @IBOutlet weak var txtFieldPassword: KdipaTextField!
    @IBOutlet weak var btnLogin: KdipaButton!
    @IBOutlet weak var btnApply: KdipaButton!
    @IBOutlet weak var lblForgetPassword: KdipaLabel!
    
    var viewModel: SignInViewModel = SignInViewModel()
    weak var coordinator: MainCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        self.txtFieldUsername.text = "adam@burgan-systems.com"
        self.txtFieldPassword.text = "Jak@3210"
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - Configure
    
    func configure() {
        
        self.txtFieldUsername.type = .username
        self.txtFieldPassword.type = .password
        self.btnApply.type = .empty
        self.btnLogin.type = .filled
        let loginTapGesture = UITapGestureRecognizer(target: self, action: #selector(login))
        self.btnLogin.button.addGestureRecognizer(loginTapGesture)
        
        let applyTapGesture = UITapGestureRecognizer(target: self, action: #selector(applyForUserName))
        self.btnApply.button.addGestureRecognizer(applyTapGesture)
        self.viewModel.loginState.bind { (state) in
            switch state {
            case .isRuning:
                self.showLoader()
            case .loginSuccess:
                self.handleLoginSuccess()
            case .loginFailure:
                self.handleLoginFailure()
            case .waitingForDashboard:
                self.getDashBoardInfo()
            default:
                self.hideSpinner(uiView: self.view)
            }
        }
    }
    
    // MARK: - Actions
    
    @objc func login() {
        self.viewModel.login(email: self.txtFieldUsername.text ?? "", password: self.txtFieldPassword.text ?? "")
    }
    
    @objc func applyForUserName() {
        coordinator?.applyForUser()
    }
    
    // MARK: - Handlers
    
    func showLoader() {
        self.showSpinner(uiView: self.view)
    }
    
    func handleLoginSuccess() {
        DispatchQueue.main.async {
            self.hideSpinner(uiView: self.view)
            self.coordinator?.dashboard()
        }
    }
    
    func handleLoginFailure() {
        DispatchQueue.main.async {
            self.hideSpinner(uiView: self.view)
            let alert = UIAlertController(title: "your email or password are wrong", message: "please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func getDashBoardInfo() {
        self.viewModel.getDashboard()
    }
}
