//
//  SignInViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
enum LoginStates {
    case notActive
    case isRuning
    case waitingForDashboard
    case loginSuccess
    case loginFailure
}

class SignInViewModel {
    
    let loginService = LoginService()
    let user: Box<LoginResponse?> = Box(nil)
    var loginState: Box<LoginStates> = Box(.notActive)
    
    var dashboard: DashboardResponse?
    
    func login(email: String, password: String) {
        self.loginState.value = .isRuning
        loginService.login(email: email, password: password) { response, error  in
            if error == nil {
                self.user.value = LoginResponse(dict: response ?? [:])
                if self.user.value?.result == "false" {
                    self.loginState.value = .loginFailure
                    return
                }
                UserDefaults.standard.set(email, forKey: Constants.UserDefaultsKeys.email)
                UserDefaults.standard.set(password, forKey: Constants.UserDefaultsKeys.password)
                self.loginState.value = .waitingForDashboard
            } else {
                self.loginState.value = .loginFailure
            }
//            switch result {
//            case .success(let user):
//                self.user.value = user
//                if user.result == "false" {
//                    self.loginState.value = .loginFailure
//                    return
//                }
//                UserDefaults.standard.set(email, forKey: Constants.UserDefaultsKeys.email)
//                self.loginState.value = .waitingForDashboard
//            case .failure(let error):
//                self.loginState.value = .loginFailure
//                debugPrint(error)
//            }
        }
    }
    
    func getDashboard() {
        loginService.getDashBoard { (response, error) in
            if error == nil {
                UserDefaults.standard.set(response, forKey: Constants.UserDefaultsKeys.dashboard)
                self.loginState.value = .loginSuccess
            } else {
                self.loginState.value = .loginFailure
            }
        }
    }
}
