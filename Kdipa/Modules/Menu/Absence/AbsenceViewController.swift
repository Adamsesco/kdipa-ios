//
//  ABsenceViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class AbsenceViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentName: KdipaLabel!
    @IBOutlet weak var currentYear: KdipaLabel!
    
    var viewModel: AbsenceViewModel = AbsenceViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.viewModel.getAbsences()
        configure()
    }
    
    func configure() {
        self.tableView.register(UINib.nib(named: VacationTableViewCell.reuseIdentifier),
                                forCellReuseIdentifier: VacationTableViewCell.reuseIdentifier)
        self.viewModel.submittingPermissionCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .success:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
                self.reconfigureVacations()
            case .failed:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
            }
        }
    }
    
    func reconfigureVacations() {
        self.currentName.text = self.viewModel.name
        self.currentYear.text = self.viewModel.year
        self.tableView.reloadData()
    }
}

extension AbsenceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.absences.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VacationTableViewCell.reuseIdentifier, for: indexPath) as? VacationTableViewCell else { return VacationTableViewCell() }
        cell.configure(row: indexPath.row, absence: self.viewModel.absences[indexPath.row])
        return cell
    }
}
