//
//  AbsenceViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class AbsenceViewModel {
    
    let service = DemandesService()
    var absence: AbsenceResponse?
    
    var submittingPermissionCallState: Box<ApiCallState> = Box(.inactive)
    
    var numberOfVacations: Int {
        return self.absence?.year["2019"]?.count ?? 0
    }
    
    var absences: [String] {
        return (self.absence?.year[self.year] ?? []).map { (absence) -> String in
            return self.formatToVacationDate(date: absence)
        }
    }
    
    var name: String {
        return (self.absence?.name ?? "") + " " + (self.absence?.nameAr ?? "")
    }
    
    var year: String {
        return self.absence?.year.keys.first ?? ""
    }
    
    func getAbsences() {
        self.submittingPermissionCallState.value = .running
        service.getAbsence { (vacationResponse, error) in
            if error == nil {
                self.absence = vacationResponse
                self.submittingPermissionCallState.value = .success
            } else {
                self.submittingPermissionCallState.value = .failed
            }
        }
    }
    
    func formatDateForDateField(date: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE dd MMMM yyyy "
        return dateFormatterPrint.string(from: date)
    }
    
    func dateFromString(dateDescription: String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE dd MMMMM hh:mm aaa"
        
        let date = dateFormatterGet.date(from: dateDescription)!
        return date
    }
    
    func formatToVacationDate(date: String) -> String {
        return formatDateForDateField(date: dateFromString(dateDescription: date))
    }
}
