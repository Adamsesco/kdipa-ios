//
//  DashboardCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class DashboardCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentCoordinator: Coordinator
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinator) {
        self.navigationController = navigationController
        self.parentCoordinator = parentCoordinator
    }
    
    func start() {
        let dashboardVC = DashboardViewController.instantiate(from: .menu)
        dashboardVC.coordinator = self
        self.navigationController.setViewControllers([dashboardVC], animated: true)
    }
    
    func newPermission() {
        (parentCoordinator as? MenuCoordinator)?.newPermission()
    }
}
