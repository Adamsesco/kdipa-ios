//
//  DashboardViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController {
    
    @IBOutlet weak var lblFullName: KdipaLabel!
    @IBOutlet weak var permissionsTakenItem: SampleItem!
    @IBOutlet weak var permissionRemainingItem: SampleItem!
    @IBOutlet weak var attendedDaysItem: ImageItem!
    @IBOutlet weak var vacationItem: ImageItem!
    @IBOutlet weak var abscenceItem: ImageItem!
    
    @IBOutlet weak var lblLastPinchIn: UILabel!
    @IBOutlet weak var lblPinchOut: UILabel!
    
    @IBOutlet weak var btnNewPermetion: UIButton!
    
    var viewModel: DashboardViewModel = DashboardViewModel()
    var coordinator: DashboardCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        setupUI()
        setupNavigationBarItems()
    }
    
    func configureContent() {
        guard let savedDashBoard = UserDefaults.standard.object(forKey: "SavedDashboard") as? [String: Any] else {
            return
        }
        let loadedDashboard = DashboardResponse(dict: savedDashBoard)
        self.lblFullName.text = loadedDashboard.nameAr
        
        self.abscenceItem.configure(type: .absence, dashBoard: loadedDashboard)
        self.vacationItem.configure(type: .vacation, dashBoard: loadedDashboard)
        self.attendedDaysItem.configure(type: .attendeddays, dashBoard: loadedDashboard)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE dd MMMMM hh:mm aaa"
        
        let lastPinchInDate = dateFormatterGet.date(from: loadedDashboard.lastPunchIn)!
        let lastPinchOutDate = dateFormatterGet.date(from: loadedDashboard.lastPunchOut)!
        
        self.lblLastPinchIn.text = dateFormatterPrint.string(from: lastPinchInDate)
        self.lblPinchOut.text = dateFormatterPrint.string(from: lastPinchOutDate)
        
        self.permissionsTakenItem.configure(type: .permissionTaken, dashboard: loadedDashboard)
        self.permissionRemainingItem.configure(type: .permisionRemaining, dashboard: loadedDashboard)
    }
    
    func setupUI() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = btnNewPermetion.bounds
        gradientLayer.colors = [UIColor.kdipaDarkBlue.cgColor, UIColor.kdipaLightBlue.cgColor]
        btnNewPermetion.layer.insertSublayer(gradientLayer, at: 0)
    }

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNewPermissionClicked(_ sender: Any) {
        self.coordinator?.newPermission()
    }
}
