//
//  EmergencyLeaveCell.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-24.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol EmergencyLeaveCellDelegate {
    func didSelect(index: Int)
}
class EmergencyLeaveCell: UICollectionViewCell {
    
    // MARK: - Type Properties
    
    static let reuseIdentifier = "EmergencyLeaveCell"
    var delegate: EmergencyLeaveCellDelegate?
    private var index: Int?
    
    // MARK: - Views
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Configure
    
    func confugire(index: Int, image: UIImage)  {
        self.index = index
        self.imageView.image = image
    }
    //MARK: - Actions
    
    @IBAction func onDelate(_ sender: Any) {
        self.delegate?.didSelect(index: self.index ?? 0)
    }
    
}
