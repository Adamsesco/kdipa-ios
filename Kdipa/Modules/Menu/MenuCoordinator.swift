//
//  MenuCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class MenuCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        BaseViewController.sideMenu.coordinator = self
        self.dashboard()
    }

    func dashboard() {
        let dashboardCoordinator = DashboardCoordinator(navigationController: self.navigationController, parentCoordinator: self)
        dashboardCoordinator.start()
    }

    func newPermission() {
        let newPermissionVc = NewPermissionViewController.instantiate(from: .menu)
        self.navigationController.setViewControllers([newPermissionVc], animated: true)
    }

    func geolocation() {
        let punchInVC = PunchInViewController.instantiate(from: .menu)
        navigationController.pushViewController(punchInVC, animated: true)
        punchInVC.navigationController?.navigationBar.isHidden = false
    }

    func openEmergencyLeave() {
         let emergencyLeaveVC = EmergencyLeaveViewController.instantiate(from: .menu)
         navigationController.pushViewController(emergencyLeaveVC, animated: true)
         emergencyLeaveVC.navigationController?.navigationBar.isHidden = false
     }
    
    func openVactions() {
        let vacationVC = VacationsViewController.instantiate(from: .menu)
        navigationController.pushViewController(vacationVC, animated: true)
        vacationVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openAbsence() {
        let absenceVC = AbsenceViewController.instantiate(from: .menu)
        navigationController.pushViewController(absenceVC, animated: true)
        absenceVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openPermission() {
        let permisionsVC = PermissionsViewController.instantiate(from: .menu)
        navigationController.pushViewController(permisionsVC, animated: true)
        permisionsVC.navigationController?.navigationBar.isHidden = false
    }
}
