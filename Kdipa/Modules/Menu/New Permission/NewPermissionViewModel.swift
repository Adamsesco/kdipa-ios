//
//  NewPermissionViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/19/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class NewPermissionViewModel {
    
    var permission: Permission = Permission()
    var isPermissionDatesSelected: Bool {
        if !permission.fromTime.isEmpty && !permission.fromTime.isEmpty && !permission.trxDate.isEmpty {
            return true
        }
        return false
    }
    
    var submittingPermissionCallState: Box<ApiCallState> = Box(.inactive)
    
    var times: [String] {
        var timesArr: [String] = [""]
        for index in 2...25 {
            let timeHour = "\(index / 2) : "
            let timeMin = (index % 2 == 0) ? "00" : "30"
            timesArr.append(timeHour + timeMin)
        }
        return timesArr
    }
    
    var services = PermissionsService()
    var istimeInAM = true
    
    func formatDateForDateField(date: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE, MMM dd "
        return dateFormatterPrint.string(from: date)
    }
    
    func formatDateForSubmiting(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let originDate = dateFormatter.date(from: String(date.split(separator: " ")[0]))!
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy/mm/dd"
        return dateFormatterPrint.string(from: originDate)
    }
    
    func submitNewPermission() {
        var submittedPermission = self.permission
        submittedPermission.trxDate = self.permission.trxDate.split(separator: " ")[0].replacingOccurrences(of: "-", with: "/")
        submittedPermission.fromTime = self.formatTimeForRequest(time: submittedPermission.fromTime)
        submittedPermission.toTime = self.formatTimeForRequest(time: submittedPermission.toTime)
        self.submittingPermissionCallState.value = .running
        services.postPermission(permission: submittedPermission) { error in
            if error == nil {
                self.submittingPermissionCallState.value = .success
            } else {
                self.submittingPermissionCallState.value = .failed
            }
        }
    }
    
    func formatTimeForRequest(time: String) -> String {
        var formattedTime = time.replacingOccurrences(of: " ", with: "")
        let hoursTime: String = String(formattedTime.split(separator: ":")[0])
        var hoursTimeInt = hoursTime.toInt() ?? 0
        if !istimeInAM && hoursTimeInt < 12 {
            hoursTimeInt += 12
            if hoursTimeInt >= 24 {
                hoursTimeInt -= 24
            }
            formattedTime = String(format: "%02d", hoursTimeInt) + ":" + formattedTime.split(separator: ":")[1]
        } else if hoursTimeInt >= 12 {
            hoursTimeInt -= 12
            formattedTime = String(format: "%02d", hoursTimeInt) + ":" + formattedTime.split(separator: ":")[1]
        }
        return formattedTime
    }
}
