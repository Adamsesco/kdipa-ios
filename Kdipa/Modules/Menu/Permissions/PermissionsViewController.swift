//
//  PermissionsViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import Charts

class PermissionsViewController: BaseViewController {

    @IBOutlet weak var currentName: KdipaLabel!
    @IBOutlet weak var pieChart: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.reconfigurePieChart()
    }
    
    func reconfigurePieChart() {
        self.currentName.text = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)
        var takenEntries: [PieChartDataEntry] = []
        takenEntries.append(PieChartDataEntry(value: 7, label: ""))
        takenEntries.append(PieChartDataEntry(value: 3, label: ""))
        let set: PieChartDataSet = PieChartDataSet(entries: takenEntries)
        set.colors = [UIColor.kdipaOrange, UIColor.kdipaPurple]
        let data: PieChartData = PieChartData(dataSet: set)
        self.pieChart.data = data
    }
}

