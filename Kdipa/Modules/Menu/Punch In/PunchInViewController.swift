//
//  PunchInViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-28.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PunchInViewController: BaseViewController {

    // MARK: - Views

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var punchInButton: UIButton!

    // MARK: - Properties
    let biometricManager = BiometricManager()
    let viewModel = PunchInViewModel()


    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItems()
        mapView.showsUserLocation = true
        userLocationAuthorizationStatus()
        configure()
//        UserLocationManager.shared.getUserAuthorization { (status) in
//            if status {
//                self.zoomToUserLocation()
//            }
//        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserLocationManager.shared.startUpdatingLocation()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: - Local Helprs

    func touchIDLoginAction() {
        
        biometricManager.authenticateUser() { [weak self] message in
            if let message = message {

                let alertView = UIAlertController(title: "Error",
                    message: message,
                    preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default)
                let appSettingAction = UIAlertAction(title: "Setting", style: .default) { (action) in
                    UIApplication.openAppSettings()

                }
                alertView.addAction(okAction)
                alertView.addAction(appSettingAction)
                self?.present(alertView, animated: true)
            } else {
                self?.viewModel.punchIn.latitude = "\(UserLocationManager.shared.currentLocation.latitude)"
                self?.viewModel.punchIn.logitude = "\(UserLocationManager.shared.currentLocation.longitude)"
                DispatchQueue.main.async {
                    self?.viewModel.submitAttendancePunch()
                }
            }
        }
    }
    
    private func configure() {
        self.viewModel.submittingPunchInCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .success:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
                self.showConfirmationMessage(view: self.view, message: "Punch In successfully sent")
            case .failed:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner(uiView: self.navigationController?.view ?? self.view)
            }
        }
    }

    private func userLocationAuthorizationStatus() {
        UserLocationManager.shared.locationStatus.bind { (status) in
            switch status {
            case .authorizedAlways:
                self.zoomToUserLocation()
            case .authorizedWhenInUse:
                self.zoomToUserLocation()
            case .restricted:
                UIApplication.openAppSettings()
            case .notDetermined:
                UserLocationManager.shared.locationManager.requestAlwaysAuthorization()
            case .denied:
                UserLocationManager.shared.locationManager.requestAlwaysAuthorization()
         
            @unknown default:
                break
            }
        }
    }
    private func zoomToUserLocation() {
        //Zoom to user location
        if let userLocation = UserLocationManager.shared.currentLocation {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 500, longitudinalMeters: 500)
               mapView.setRegion(viewRegion, animated: false)
            UserLocationManager.shared.stopUpdatingLocation()
           }
    }

    // MARK: - Actions

    @IBAction func onPunchIn(_ sender: Any) {
        touchIDLoginAction()
    }

}
