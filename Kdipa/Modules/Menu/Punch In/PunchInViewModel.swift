//
//  PunchInViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-28.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class PunchInViewModel {
    
    var submittingPunchInCallState: Box<ApiCallState> = Box(.inactive)
    var punchIn: PunchIn = PunchIn()
    var services = PunchInService()


    init() {}
    
    func submitAttendancePunch() {
        self.submittingPunchInCallState.value = .running

        services.postPunchIn(punchIn: self.punchIn) { value,error  in
            if error == nil {
                self.submittingPunchInCallState.value = .success
            } else {
                self.submittingPunchInCallState.value = .failed
            }
        }
    }

}
