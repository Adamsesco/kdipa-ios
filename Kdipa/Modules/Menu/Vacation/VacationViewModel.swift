//
//  VacationViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class VacationViewModel {
    
    let service = DemandesService()
    var vacation: VacationResponse?
    
    var submittingPermissionCallState: Box<ApiCallState> = Box(.inactive)
    
    var numberOfVacations: Int {
        return self.vacation?.year["2019"]?.count ?? 0
    }
    
    var vacations: [Vacation] {
        return (self.vacation?.year[self.year] ?? []).map { (vacation) -> Vacation in
            let newVacation = Vacation(
                dateFrom: self.formatToVacationDate(date: vacation.dateFrom),
                dateTo: self.formatToVacationDate(date: vacation.dateTo),
                nameAr: vacation.nameAr,
                nameEn: vacation.nameEn,
                period: vacation.period)
            return newVacation
        }
    }
    
    var name: String {
        return (self.vacation?.name ?? "") + " " + (self.vacation?.nameAr ?? "")
    }
    
    var year: String {
        return self.vacation?.year.keys.first ?? ""
    }
    
    func getVacations() {
        self.submittingPermissionCallState.value = .running
        service.getVacations { (vacationResponse, error) in
            if error == nil {
                self.vacation = vacationResponse
                self.submittingPermissionCallState.value = .success
            } else {
                self.submittingPermissionCallState.value = .failed
            }
        }
    }
    
    func formatDateForDateField(date: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE, MMM dd "
        return dateFormatterPrint.string(from: date)
    }
    
    func dateFromString(dateDescription: String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE dd MMMMM hh:mm aaa"
        
        let date = dateFormatterGet.date(from: dateDescription)!
        return date
    }
    
    func formatToVacationDate(date: String) -> String {
        return formatDateForDateField(date: dateFromString(dateDescription: date))
    }
}
