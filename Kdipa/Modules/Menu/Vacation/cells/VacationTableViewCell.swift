//
//  VacationTableViewCell.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class VacationTableViewCell: UITableViewCell {

    static let reuseIdentifier = "VacationTableViewCell"
    
    @IBOutlet weak var itemView: KdipaPermissionItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(row: Int, title: String, date: String) {
        itemView.configure(row: row, title: title, date: date)
    }
    
    func configure(row: Int, vacation: Vacation) {
        itemView.configure(row: row + 1, title: vacation.nameEn, date: vacation.dateFrom)
    }
    
    func configure(row: Int, absence: String) {
        itemView.configure(row: row + 1, title: absence, date: "")
    }
}
