//
//  RemoteAPI.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

enum KdipaAPI: APIProtocol {
    case login
    case applyForUser
    case dashboard
    case vacations
    case absence
}

extension KdipaAPI {
    
    var employeeBaseURL: URL {
        return URL(string: Constants.kdipaEmployeeBaseURL)!
    }
    var investorBaseURL: URL {
        return URL(string: Constants.kdipaInvestorBaseURL)!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/api/login"
        case .applyForUser:
            return "/api/applyForUsername"
        case .dashboard:
            return "/api/dashboard"
        case .absence:
            return "/api/historyAbsent"
        case .vacations:
            return "/api/historyVacation"
        }
    }
    
}
