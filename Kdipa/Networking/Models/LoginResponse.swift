//
//  User.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    var firstName: String
    var lastName: String
    var result: String
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case result = "Result"
    }
    
    init(dict: [String: Any]) {
        self.firstName = dict["FirstName"] as? String ?? ""
        self.lastName = dict["LastName"] as? String ?? ""
        self.result = dict["Result"] as? String ?? ""
    }
}
