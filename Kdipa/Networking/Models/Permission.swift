//
//  Permission.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/19/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Permission: Codable {
    var email: String = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
    var trxDate: String = ""
    var permissionType: String = ""
    var fromTime: String = ""
    var toTime: String = ""
    var filename: Data?
}
