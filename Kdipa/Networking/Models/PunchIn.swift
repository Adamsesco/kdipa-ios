//
//  PunchIn.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-29.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct PunchIn: Codable {
    var email: String = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
    var password: String = ""
    var latitude: String?
    var logitude: String?
}

