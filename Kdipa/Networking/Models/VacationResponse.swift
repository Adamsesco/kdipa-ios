//
//  VacationResponse.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - Welcome

struct VacationResponse: Codable {
    let name, nameAr: String
    let year: [String: [Vacation]]
}

// MARK: - Vacation

struct Vacation: Codable {
    var dateFrom, dateTo, nameAr, nameEn: String
    let period: Int

    enum CodingKeys: String, CodingKey {
        case dateFrom = "DateFrom"
        case dateTo = "DateTo"
        case nameAr = "Name-Ar"
        case nameEn = "Name-En"
        case period = "Period"
    }
}
