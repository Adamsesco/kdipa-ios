//
//  LoginService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

class LoginService {
    
    func login(email: String,
               password: String,
               completion: @escaping ([String: String]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.login.path)!
        let params = ["email": email, "password": password, "token": "", "device_type": "IOS"]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                completion(value as? [String: String], nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func getDashBoard(completion: @escaping ([String: Any]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.dashboard.path)!
        let params = ["email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)!]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dashboard = value as? [String: Any]
                completion(dashboard, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
