//
//  PunchInService.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-29.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

import Alamofire

fileprivate let punchInEndPoint = "http://216.250.119.123:3001/api/geoAttend"

class PunchInService {
    
    func postPunchIn(punchIn: PunchIn, completion: @escaping ([String: Any]?, Error?) -> Void) {
        let parameters = punchIn.dictionary as? [String: String] ?? [:]
          let headers = ["Content-Type": "application/json", "Accept": "application/json"]
          Alamofire.request(punchInEndPoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
              switch response.result {
              case .success(let value):
                  let dashboard = value as? [String: Any]
                  completion(dashboard, nil)
              case .failure(let error):
                  completion(nil, error)
              }
          }
    }
}
