//
//  UserService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct UserService {
    func createUser(user: User, completion: @escaping ([String: Any]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaInvestorBaseURL + KdipaAPI.applyForUser.path)!
        let headers = ["Content-Type": "application/json"]
        Alamofire.request(url, method: .post, parameters: user.dictionary, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                completion(value as? [String: Any] ?? [:], nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
